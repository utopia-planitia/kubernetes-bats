#!/usr/bin/env bats

setup() {
  kubectl delete -f dns --ignore-not-found=true
  kubectl delete po -l=job-name=google-public-dns-a-google-com
  kubectl delete po -l=job-name=node-local-dns-ip
  kubectl delete po -l=job-name=kubernetes-default-svc-cluster-local
  kubectl delete po -l=job-name=kubernetes-default-svc
  kubectl delete po -l=job-name=kubernetes-default
  kubectl delete po -l=job-name=kubernetes
}

teardown () {
  kubectl delete -f dns --ignore-not-found=true
  kubectl delete po -l=job-name=google-public-dns-a-google-com
  kubectl delete po -l=job-name=node-local-dns-ip
  kubectl delete po -l=job-name=kubernetes-default-svc-cluster-local
  kubectl delete po -l=job-name=kubernetes-default-svc
  kubectl delete po -l=job-name=kubernetes-default
  kubectl delete po -l=job-name=kubernetes

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check google-public-dns-a-google-com ip" {
  kubectl apply -f dns/google-public-dns-a.google.com.yml
  run kubectl wait --for=condition=complete --timeout=60s job/google-public-dns-a-google-com
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod -l=job-name=google-public-dns-a-google-com --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [ "${lines[1]}" = "Address: 8.8.8.8" ]
}

@test "check dns service ip" {
  kubectl apply -f dns/dns-service-ip.yml
  run kubectl wait --for=condition=complete --timeout=60s job/node-local-dns-ip
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod --selector=job-name=node-local-dns-ip --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${lines[0]}" = "Address:	10.96.0.10#53" ]
}

@test "check kubernetes-default-svc-cluster-local ip" {
  kubectl apply -f dns/kubernetes.default.svc.cluster.local.yml
  run kubectl wait --for=condition=complete --timeout=60s job/kubernetes-default-svc-cluster-local
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod --selector=job-name=kubernetes-default-svc-cluster-local --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [ "${lines[1]}" = "Address: 10.96.0.1" ]
}

@test "check kubernetes-default-svc ip" {
  kubectl apply -f dns/kubernetes.default.svc.yml
  run kubectl wait --for=condition=complete --timeout=60s job/kubernetes-default-svc
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod --selector=job-name=kubernetes-default-svc --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [ "${lines[1]}" = "Address: 10.96.0.1" ]
}

@test "check kubernetes-default ip" {
  kubectl apply -f dns/kubernetes.default.yml
  run kubectl wait --for=condition=complete --timeout=60s job/kubernetes-default
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod --selector=job-name=kubernetes-default --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [ "${lines[1]}" = "Address: 10.96.0.1" ]
}

@test "check kubernetes ip" {
  kubectl apply -f dns/kubernetes.yml
  run kubectl wait --for=condition=complete --timeout=60s job/kubernetes
  [ $status -eq 0 ]
  run kubectl logs `kubectl get pod --selector=job-name=kubernetes --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [ "${lines[1]}" = "Address: 10.96.0.1" ]
}
