#!/usr/bin/env bats

@test "use host port" {
  kubectl apply -f host-port
  sleep 1
  run kubectl -n test-host-port wait pods --timeout=60s -l app=nginx --for=condition=ready
  [ $status -eq 0 ]
  
  PORT=32123
  {{ range $index, $node := .Nodes}}
  run curl -v --connect-timeout 5 --max-time 5 --retry 12 --retry-delay 0 --retry-max-time 60 --retry-connrefused http://{{ $node.Network.PublicIP }}:${PORT}/
  [ $status -eq 0 ]
  {{end}}
}

teardown () {
  kubectl delete -f host-port --ignore-not-found=true

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}
