#!/usr/bin/env bats

@test "use node port" {
  kubectl apply -f node-port
  sleep 1
  run timeout 120 sh -c "until kubectl -n test-node-port wait pods -l app=nginx --for=condition=ready; do sleep 1; done"
  [ $status -eq 0 ]

  PORT=`kubectl get svc -n test-node-port -o go-template='{{`{{range .items}}{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}{{end}}`}}'`
  {{ range $index, $node := .Nodes}}
  run curl -v --connect-timeout 5 --max-time 5 --retry 12 --retry-delay 0 --retry-max-time 60 --retry-connrefused http://{{ $node.Network.PublicIP }}:${PORT}/
  [ $status -eq 0 ]
  {{end}}
}

teardown () {
  kubectl delete -f node-port --ignore-not-found=true

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}
