#!/usr/bin/env bats

load test_helper

{{ range $index, $node := .Nodes.SelectMasters}}

@test "verify kubernetes version {{ $node.Name }}" {
  run kubectl --server=https://{{ $node.Network.PublicIP }}:6443/ version
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 2 ]
  [[ "${lines[0]}" =~ ^Client\ Version:\ .* ]]
  [[ "${lines[1]}" =~ ^Server\ Version:\ .* ]]
}

{{end}}
