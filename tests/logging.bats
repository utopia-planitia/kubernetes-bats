#!/usr/bin/env bats

setup() {
  kubectl delete -f logging --ignore-not-found=true
  kubectl delete po -l job-name=hello-world
}

teardown () {
  kubectl delete -f logging --ignore-not-found=true
  kubectl delete po -l job-name=hello-world

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check logging hello, world" {
  kubectl apply -f logging
  run kubectl wait --for=condition=complete --timeout=60s job/hello-world
  [ $status -eq 0 ]

  run kubectl logs `kubectl get pod --selector=job-name=hello-world --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 1 ]
  [ "${lines[0]}" = "Hello, World." ]
}
