#!/usr/bin/env bats

load test_helper

{{ range $index, $node := .Nodes.SelectMasters}}

@test "verify etcdctl {{ $node.Name }}" {
  run kubectl -n kube-system exec -c etcd etcd-{{ $node.Name }} -- etcdctl member list
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 3 ]
}

{{end}}
