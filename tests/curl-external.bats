#!/usr/bin/env bats

setup() {
  kubectl delete -f curl-external/ --ignore-not-found=true
  kubectl delete po -l job-name=unicorn
  kubectl delete po -l job-name=google-com-ipv4
}

teardown () {
  kubectl delete -f curl-external/ --ignore-not-found=true
  kubectl delete po -l job-name=unicorn
  kubectl delete po -l job-name=google-com-ipv4

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "curl a unicorn" {
  kubectl apply -f curl-external/unicorn.yml
  run kubectl wait --for=condition=complete --timeout=120s job/unicorn
  [ $status -eq 0 ]

  run kubectl logs `kubectl get pod --selector=job-name=unicorn --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 1 ]
  [ "${lines[0]}" = "                      ==/   /O   O\==--" ]
}

@test "curl IPv4 google com" {
  kubectl apply -f curl-external/google-com-ipv4.yml
  run kubectl wait --for=condition=complete --timeout=120s job/google-com-ipv4
  [ $status -eq 0 ]
}
