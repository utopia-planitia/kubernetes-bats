#!/usr/bin/env bats

@test "check transfer time" {
  kubectl apply -f network-throughput
  run kubectl wait --for=condition=ready --timeout=60s pod/transmit
  [ $status -eq 0 ]
  run kubectl wait --for=condition=ready --timeout=60s pod/receive
  [ $status -eq 0 ]

  run kubectl exec transmit -- apk --update add curl
  [ $status -eq 0 ]

#  kubectl exec transmit -- dd if=/dev/urandom of=/file bs=1M count=100
  run kubectl exec transmit -- timeout -t 2 curl -o file --fail https://speed.hetzner.de/100MB.bin
  [ $status -eq 0 ]

  run kubectl exec transmit -- timeout -t 60 sh -c "until curl --fail --max-time 1 --connect-timeout 1 http://receive/; do sleep 1; done"
  [ $status -eq 0 ]

  positive=0
  for i in {1..10}; do
    echo "run #${i}"
    # timeout -t 2 ensures at least 66,5MB/s
    if kubectl exec transmit -- timeout -t 2 curl -T file --fail http://receive/file; then
      let "positive=positive+1"
    fi
  done

  [ $positive -ge 9 ] # 9 or 10 out of 10
}

teardown () {
  kubectl delete -f network-throughput --ignore-not-found=true

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}
