#!/usr/bin/env bats

setup () {
  kubectl delete -f ping --ignore-not-found=true
  kubectl delete po --selector=job-name=google-dns
}

teardown () {
  kubectl delete -f ping --ignore-not-found=true
  kubectl delete po --selector=job-name=google-dns

# from "load test_helper"
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "check ping to google-dns" {
  run kubectl apply -f ping
  [ $status -eq 0 ]

  run kubectl wait --for=condition=complete --timeout=60s job/google-dns
  [ $status -eq 0 ]

  run kubectl logs `kubectl get pod --selector=job-name=google-dns --output=jsonpath={.items..metadata.name}`
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 5 ]
  [ "${lines[3]}" = "1 packets transmitted, 1 packets received, 0% packet loss" ]
}
