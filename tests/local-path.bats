#!/usr/bin/env bats

setup() {
  kubectl delete job/local-path-test-read job/local-path-test-write --ignore-not-found=true
  kubectl delete po -l job-name=local-path-test-read
  kubectl delete po -l job-name=local-path-test-write
  kubectl delete -f local-path/ --ignore-not-found=true
  kubectl get pv | grep Released | grep local-path | awk '{ print $1 }' | xargs --no-run-if-empty -L 1 kubectl delete --ignore-not-found=true pv
}

teardown() {
  kubectl delete job/local-path-test-read job/local-path-test-write --ignore-not-found=true
  kubectl delete po -l job-name=local-path-test-read
  kubectl delete po -l job-name=local-path-test-write
  kubectl delete -f local-path/ --ignore-not-found=true
  kubectl get pv | grep Released | grep local-path | awk '{ print $1 }' | xargs --no-run-if-empty -L 1 kubectl delete --ignore-not-found=true pv

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "write to and read from persistent local volume" {
  run kubectl apply -f local-path/volume.yml
  [ $status -eq 0 ]

  run kubectl apply -f local-path/volume-write.yml
  [ $status -eq 0 ]
  sleep 1

  run kubectl -n default wait --for=condition=complete --timeout=60s job/local-path-test-write
  [ $status -eq 0 ]

  run kubectl apply -f local-path/volume-read.yml
  [ $status -eq 0 ]
  sleep 1

  run kubectl -n default wait --for=condition=complete --timeout=60s job/local-path-test-read
  [ $status -eq 0 ]
}
