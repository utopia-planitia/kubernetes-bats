#!/usr/bin/env bats

load test_helper

{{ range $index, $node := .Nodes.SelectMasters}}

@test "verify componentstatus {{ $node.Name }}" {
  run diff componentstatus/request.golden <(kubectl --server=https://{{ $node.Network.PublicIP }}:6443/ get componentstatus -o yaml | grep status:)
  [ $status -eq 0 ]
  [ "${#lines[@]}" -eq 0 ]
}

{{end}}
